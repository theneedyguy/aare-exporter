FROM alpine:latest as alpine
RUN apk add -U --no-cache ca-certificates


FROM busybox
LABEL maintainer="CKEVI <admin@0x01.host>"

COPY --from=alpine /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY aare-exporter /bin/aare-exporter

USER nobody
EXPOSE 3005
ENTRYPOINT [ "/bin/aare-exporter" ]

